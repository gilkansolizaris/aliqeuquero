﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

	public float speed;
	public float rotateSpeedMod; // 0 = stop / 0<x<1 - slower / 1 = nothing changes / x>1 quicker
	public GameObject destination;
	public GameObject origin;

	private Vector3 dimensionVector;
	private bool going;
	private int goingAux;
	private bool clicked;
	private bool running;

	private bool forceExit = false;

	void OnApplicationPause(bool pause)
	{
		if (pause) forceExit = true;
	}

	// Use this for initialization
	void Start () {
		goingAux = -1;
		running = false;
		going = true;
	}

	// Update is called once per frame
	void Update () {
		if (forceExit) {
			Application.Quit ();
		}
		if(this.transform.position==destination.transform.position && going){
			running = false;
			going = !going;
			goingAux *= -1;
			Debug.Log ("Coliding 1");
		}
		if(this.transform.position==origin.transform.position && !going){
			running = false;
			going = !going;
			goingAux *= -1;
			Debug.Log ("Coliding 2");
		}
		if(running){
			if(going){
				this.transform.position = Vector3.MoveTowards (this.transform.position,destination.transform.position,speed);
				this.transform.Rotate (Vector3.forward * speed * rotateSpeedMod * goingAux);
			}else{
				this.transform.position = Vector3.MoveTowards (this.transform.position,origin.transform.position,speed);
				this.transform.Rotate (Vector3.forward * speed * rotateSpeedMod * goingAux);
			}
		}
	}

	public void setClicked(){
		if(!running){
			running = true;
		}
	}
}
