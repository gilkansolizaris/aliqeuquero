﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAndHide : MonoBehaviour {

	public List<GameObject> objs;
	private bool clicked;
	private bool isShowing;

	// Use this for initialization
	void Start () {
		isShowing = true;
		clicked = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(clicked){
			isShowing = !isShowing;
			foreach(GameObject obj in objs){
				obj.SetActive (isShowing);
			}
			clicked = false;
		}
	}

	public void setClicked(){
		clicked = true;
	}
}
