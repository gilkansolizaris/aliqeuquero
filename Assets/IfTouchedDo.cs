﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class IfTouchedDo : MonoBehaviour {
	private Ray ray;
	private RaycastHit hit;
	int mask = ~(1 << 2);
		
	// Update is called once per frame
	void Update () {
		foreach( Touch touch in Input.touches ) {

			if( touch.phase == TouchPhase.Began ) {

				ray = Camera.main.ScreenPointToRay(touch.position);

				if (Physics.Raycast (ray.origin, ray.direction, out hit, Mathf.Infinity, mask)) {
						Debug.Log ("Touch1: Clicked: "+hit.transform.gameObject.name);
					if (hit.transform.gameObject.GetComponent<Move> () != null) {
						Move script = hit.transform.gameObject.GetComponent<Move> ();
						script.setClicked ();
					}
					if(this.GetComponent<ShowAndHide> () != null){
						ShowAndHide script = this.GetComponent<ShowAndHide> ();
						script.setClicked ();
					}
				}
			}
		}
	}
}
